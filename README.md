# NMAid - Notify My Android - I'm Done

cli-tool to send messages to your mobile device. Useful when waiting for a command to finish

## Usage

## Examples

~~~~
./slow-command && nmaid slow-command finally done!
~~~~