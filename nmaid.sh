#!/bin/bash
##
# Notify android.
##
set -e

which curl >/dev/null  || { errormessage="curl is required but it's not installed, exiting."; error_exit; }

usage(){
    echo "Usage: $0 [ -a application -p priority ( -2 to 2] message";
}


if [ -f ~/.nma ]; then
    . ~/.nma
else
    echo "No config is found";
    exit -1;
fi;

if [[ "${#APIkey}" -ne "48" ]]; then
    echo "APIkey not found";
fi;

application="[nmaid]";
event=$(hostname);
priority=0
message="";
verbose=1;

while getopts ha:p: o;do
    case "$o" in
	h) usage; exit 0;;
	a) application="$OPTARG" ;;
	p) priority="$OPTARG";;
    esac
done

shift $(($OPTIND - 1))
while [ $# -gt 0 ]; do
    message="$message $1"
    shift
done;

baseurl="https://www.notifymyandroid.com/publicapi"
verifyurl="$baseurl/verify"
posturl="$baseurl/notify"

if [ "$message" == "" ]; then
    if test -t 0; then
	message=$message;
    else
	while read line; do
	    message="$message ${line}"
	done < /dev/stdin
    fi;
fi;

if [ $verbose -gt 0 ]; then
    echo  -en "Sending app: $application\nevent:$event\nmessage:$message\n";
fi;


### Input check ###
if [[ "${#application}" -gt "256" ||  "${#application}" -lt "1" ]]; then
    echo "[ error ] the application parameter is invalid or missing"
    exit -1
elif [[ "${#event}" -gt "1000"  ||  "${#event}" -lt "1" ]]; then
    echo "[ error ] the event parameter is invalid or missing"
    exit -1
elif [[ "${#message}" -gt "1000"  ||  "${#message}" -lt "1" ]]; then
    echo "[ error ] the message parameter is invalid or missing"
    exit -1
fi

if [ -z $priority ]; then
    printf '%s\n' "priority is not set , must be between -2 and 2, using 0 instead"
    priority=0
elif [[ "$priority" -gt "2" || $priority -lt "-2" ]]; then
    printf '%s\n' "priority $priority is invalid, must be between -2 and 2, using 0 instead"
    priority=0
fi

# create file to post
notifyout=/tmp/androidNotify$(date '+%d%m%Y%H%M%S%N')
printf '\n%s\n' "$application , $event , $message, priority=$priority" >> $notifyout ##adding info used to tmp file. 

# and send off to nma service
curl --silent --data-ascii "apikey=$APIkey" --data-ascii "application=$application" --data-ascii "event=$event" --data-ascii "description=$message" --data-asci "priority=$priority" $posturl -o $notifyout && complete=1

rm $notifyout;

#done!
